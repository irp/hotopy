import torch
import numpy as np
from torch import nn
from scipy.io import loadmat

from hotopy.algorithms import FADMM
from hotopy.phase.ctf import Constraints, TorchCTF
from hotopy.phase.util import croppad


class CtfModel(nn.Module):
    """
    ML 'model' for torch; actually we just need the parameter as the forward model is completely written as
    proximal operators for CTF and constrains
    """
    def __init__(self, padshape, padding, imdim: int = 2, device: torch.device = None):
        super(CtfModel, self).__init__()
        # TODO: this could be 'hot' started with ctf reconstruction?
        self.x = nn.Parameter(torch.zeros(padshape).to(device), requires_grad=False)
        #if device is not None:
        #    self.x = self.x.to(device)
        self.padding = padding
        self.imdim = imdim

    def forward(self):
        # within fADMM stepping no padding/cropping is performed. To get final result call instance of CtfModel.
        return self.x[croppad(self.padding[-self.imdim:])]


# load example data
#with loadmat('holos_dicty.mat') as fdata:
data = loadmat('tests/debug_matlab_ctf/dicty-holos.mat')
holos = np.moveaxis(data['holograms'], -1, 0)
fresnelNums = data['fresnelNumbers'][0]

# padding
padshape = (np.ceil(np.asarray(holos.shape[-2:]) + 1024)).astype(int)
print(f'image {holos.shape[-2:]} padded to {padshape}')

ctf = TorchCTF(holos.shape[-2:], fresnelNums, alpha = [1e-4, 1e-2], betadelta=0.01, device='cpu', padshape=padshape)
ctf_model = CtfModel(ctf.padshape, ctf.padding, device=ctf._device)

# constrains = Constrains() # negativity constrain
constrains = Constraints(phase_max=0) # negativity constrain
# constrains = Constrains(phase_max=0, phase_min=-1)

### from here we need the data: i.e. this is the __call__ for directCTF
prox_ctf = ctf.as_proximal_operator(holos)
prox_constrains = constrains.as_proximal_operator()
proxs = [prox_constrains, prox_ctf] # NOTE: the order of prox-ops does not seems to be important as the fADMM is constrained with u=v

fadmm = FADMM(ctf_model.parameters(), proxs, torch.zeros(ctf.padshape).to(ctf._device), lr=.01, verbose=True)

for i in range(50):
    resi_prim, resi_dual = fadmm.step()
    print(f'{i}: residuals (primal, dual) = {resi_prim:.2e}, {resi_dual:.2e}')
    if max(resi_prim, resi_dual) < 1e-3:
        print(f'Converged!')
        break

result = ctf_model().cpu()
#torch.save(result, 'tests/debug_matlab_ctf/fadmm_dicty-1iter.dat')

# in HoloTomoToolbox the u variable is used as result. Access via state dict.
u = fadmm.state_dict()['state'][0]['u']
u_crop = u[croppad(ctf.padding[-2:])]
#torch.save(u_crop, 'tests/debug_matlab_ctf/fadmm_dicty_u.dat')

compare = ctf(holos).cpu()
#torch.save(compare, 'compare_dicty.dat')

