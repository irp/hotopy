import torch
from torch.nn import Module, Parameter, Threshold
from torch.optim import SGD
from hotopy.algorithms import PGD, stepsize_barlizai_borwein


def P(x, p, q):
    return x ** 2 + p * x + q


class PolynomialModel(Module):
    def __init__(self, x0, p, q):
        super().__init__()
        self.x = Parameter(x0)
        self.p = p
        self.q = q

    def forward(self):
        return P(self.x, self.p, self.q)


IdOp = lambda x: x

tol = 1e-3
max_iter = 50
x0 = torch.tensor(-1.)
p = -2.
q = 1.
τ = 0.05

pm = PolynomialModel(x0, p, q)
error_fn = lambda : pm()

# compare with SGD
sgd = SGD(pm.parameters(), τ)
for i in range(max_iter):
    y = error_fn()
    ss = sgd.param_groups[0]["lr"]
    print(f"SGD {i}: y = {y:.3e}, x = {pm.x:.3f}, ss = {ss:.3e}")

    if y < tol:
        break

    sgd.zero_grad(True)
    y.backward()
    sgd.step()


# initialize
pm = PolynomialModel(torch.tensor(-1.), p, q)
solver = PGD(list(pm.parameters()),
             [IdOp],
             error_fn,
             τ,
             ls_backtracking=1)
error_fn = lambda : pm()
err = error_fn()
err.backward()
solver._update_backtracking(err)

# iterate
for i in range(max_iter):
    print(f"iteration {i}: err = {err:.3e} x = {pm.x:.3f}, ss = {solver.stepsize:.3e}")

    if err < tol:
        print(f'done in {i} steps')
        break

    err = solver.step()
    pm.x.grad = None
    err.backward()
else:
    print("tolerance not reached")
