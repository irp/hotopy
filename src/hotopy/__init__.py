"""
=============================================
HoToPy: A Holo- and Tomography Python Toolbox
=============================================

To use any submodule an explicit import is required either via
 - ``import hotopy.holo`` or
 - ``from hotopy import holo`` or
 - ``from hotopy.holo import *``
or the used functions are imported explicitly ``from hotopy.holo import CTF``.

Submodules
-------

.. autosummary::
    :toctree: generated/

    holo
    tomo
    image
    datasets
    xray

Optimization
------------

.. autosummary::
    :toctree: generated/

    optimize


Utilities and helpers
----------------------

.. autosummary::
    :toctree: generated/

    utils

"""
