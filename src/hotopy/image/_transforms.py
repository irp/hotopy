from numpy import ones, diag, asarray, ascontiguousarray
from numpy import linalg
from scipy.ndimage import affine_transform, shift as ndshift


def imscale(input, scale, center=True, mode="nearest", **kwargs):
    """
    Scales an image using an affine transformation.

    See `scipy.ndimage.affine_transform` [1]_.

    Parameters
    ----------
    input : array
        Array to zoom.
    scale : float, tuple
        Scale factor per dimension or scalar if same along all directions.
    center : bool, optional
        Zoom into center of the image. Defaults to `True`, *which is different from `affine_transform` standard
        behavior*.
        Set offset argument to zoom into different regoins in the array.
    mode : str, optional
        Change default value of affine_transform to `'nearest'`.
    **kwargs: optional
        any keyword argument affine_transform supports

    Returns
    -------
    Scaled image with same shape as entered image `im`.

    Notes
    -----
    Scaling or maginfication of an image is different than zooming (in scipy's sense)
    as when scaled the shape of the image does not change, if zoomed, e.g. with
    `scipy.ndimage.zoom` the shapes does change.

    Example
    -------
    >>> from hotopy.image import imscale
    >>> from scipy.misc import ascent
    >>> im = ascent()
    >>> zoomin = imscale(im, 2)
    >>> zoomout = imscale(im, (1/2, 1))

    References
    ----------
    .. [1] https://docs.scipy.org/doc/scipy/reference/generated/scipy.ndimage.affine_transform.html#scipy-ndimage-affine-transform
    """
    # early exit, if there is nothing to do
    if scale == 1:
        return input

    if center and "offset" in kwargs and kwargs["offset"] is not None:
        raise ValueError(
            "Conflicting arguments passed to function: explicit offset and auto-centering are mutually exclusive."
        )
    offset = kwargs.pop("offset") if "offset" in kwargs else None

    input = ascontiguousarray(input)
    scale = scale * ones(input.ndim)

    # inverse of affine transformation matrix needs to be passed to affine_transform. Inverse of "diagonal" elements
    # or diagonal matrix is equal.
    inv = 1 / scale

    # zoom into center
    if center:
        offset = asarray(input.shape) / 2 * (1 - inv)

    return affine_transform(input, inv, offset=offset, mode=mode, **kwargs)


def imshift(input, shift, mode="nearest", **kwargs):
    """
    Wrapper for `scipy.ndimage.shift` with different edge behavior default of `'nearest'`.

    Parameters
    ----------
    input : array
        Array to shift.
    shift : array
        Shifts per dimension.

    Return
    ------
    shift : array
        Shifted and interpolates image.
    """
    return ndshift(input, shift, mode=mode, **kwargs)


def _center_offset(shape, scale):
    return asarray(shape) / 2 * (1 - 1 / asarray(scale))


def imshiftscale(input, shift, scale, center=True, offset=None, mode="nearest", **kwargs):
    """
    Jointly shifts and scales an array (i.e. image) using only one interpolation step.
    This should avoid image degradation if done in consecutive operations.

    Internaly an affine transformation using homogeneous coordinates is set up. See `scipy.ndimage.affine_transform`
    [1]_ and [2]_ for further details.

    Parameters
    ----------
    input : array
        Array to scale.
    shift : float, tuple
        Shift per dimension or scalar if same along all directions.
    scale : float, tuple
        Scale factor per dimension or scalar if same along all directions.
    center : bool, optional
        Zoom into center of the image. Defaults to `True`, *which is different from `affine_transform` standard
        behavior*.
        Set offset argument to zoom into different regoins in the array.
    offset : float or sequence, optional
        Offset to perform scale operations on. If `center=True` offset is determined from center!
    mode : str, optional
        Change default value of affine_transform to `'nearest'`.
    **kwargs: optional
        any keyword argument affine_transform supports

    Returns
    -------
    array
        Scaled and shifted image in same shape as `input`.

    Notes
    -----
    Use `imscale` or `imshift` if only one operation is to perform to exploit more efficient algorithms eventually.

    Scaling or maginfication of an image is different than zooming (in scipy's sense)
    as when scaled the shape of the image does not change, if zoomed, e.g. with
    `scipy.ndimage.zoom` the shapes does change.

    Example
    -------
    >>> from hotopy.image import imshiftscale
    >>> from scipy.misc import ascent
    >>> im = ascent()
    >>> transformed = imshiftscale(im, [50, -20.25], 1.25, mode="reflect")

    References
    ----------
    .. [1] https://docs.scipy.org/doc/scipy/reference/generated/scipy.ndimage.affine_transform.html#scipy-ndimage-affine-transform

    .. [2] https://en.wikipedia.org/wiki/Homogeneous_coordinates

    .. [3] https://en.wikipedia.org/wiki/Affine_transformation#Image_transformation
    """
    ndim = input.ndim

    # scaling along each axis (diagonal matrix) in homogeneous coordinates
    scale = scale * ones(ndim)
    scale_op = ones(ndim + 1)
    scale_op[:-1] = scale
    op = diag(scale_op)

    # translations/shifts with offset to center
    offset = offset or 0
    if center:
        offset = _center_offset(input.shape, scale)
    op[:-1, -1] = shift * ones(ndim) - asarray(offset) * scale

    # TODO add rotations?

    # inverse transformation needs to be passed to affine_transform
    inv = linalg.inv(op)

    return affine_transform(input, inv, mode=mode, **kwargs)
