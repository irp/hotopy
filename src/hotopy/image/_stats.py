import numpy as np
from scipy.stats import binned_statistic
from scipy.fft import fftn

from ..utils import fftfreqn
from . import ndwindow


def radial_profile(a, center=None, bin_width=1, bins=None):
    """
    Calculates the radial profile of an n-dimesional input a by azimuthal averaging.

    a: array
        array to calculate the radial average of
    center: tuple (optional)
        center in image coordinates. Defaults to the image center.
    bin_width: number (optional)
        Width of radial bins to use. Ignored, if `bins` is given. Default is 1.
    bins: array (optional)
        Radial bins to use.

    Returns
    -------
    average: array
        mean value per radial bin
    radii: array of dtype float
        radial bin edges
    binnumber:
        indices of the bin, in which each value belongs.
    """
    a = np.asarray(a)
    shape = np.asarray(a.shape)
    if center is None:
        center = (shape - 1) / 2

    xv = np.indices(shape) - center[..., np.newaxis, np.newaxis]
    r = np.sqrt((xv**2).sum(0))

    if bins is None:
        bins = np.arange(0, r.max() + bin_width, bin_width)

    return binned_statistic(r.ravel(), a.ravel(), bins=bins)


def radial_power_spectrum(im, window=("kaiser", 8)):
    """
    Azimuthal averaged power spectral density of image im.

    Parameters
    ----------
    im: array-like
        Image to compute power spectrum of. Image will be cut quadratic and radial average is performed over all
        dimensions.
    window: str, tuple, Optional
        Optionally apply windowing before calculation of power spectrum to suppress boundary effects. See
        scipy.signal.get_window window argument for possible values.
        Defaults to `("kaiser", 8)`. Set to `None` if no windowing should be applied.

    Returns
    -------
    psd, freq, binn:
        power spectrum, corresponding fourier frequency (of length 1), pixel to radii (bin) mapping

    Example
    -------
    >>> psd, freq, binn = radial_power_spectrum(im, ("kaiser", 8))
    """
    if window is not None:
        im = im * ndwindow(window, im.shape)

    # compute FFT and move zero to center of image by fftshift
    im_fft = np.fft.fftshift(np.fft.fftn(im))
    zero_freq = np.array(im_fft.shape) // 2
    power_spectral_density = np.square(abs(im_fft))
    bins = np.arange(np.max(im_fft.shape) / 2)
    psd, radii, bins = radial_profile(power_spectral_density, center=zero_freq, bins=bins)
    return psd, radii[:-1] / im.shape[0], bins


def fourier_shell_correlation(im1, im2, window=("kaiser", 8), dfreq=None):
    """
    Fourier shell correlation (FSC) of n-dim inputs im1 and im2.

    Parameters
    ----------
    im1, im2: array-like
        n-dimensional images to compute the FSC of.
    window: (optional)
        Apply windowing before calculation of fourier transform to suppress boundary effects.
        See scipy.signal.get_window window argument for possible values.
        Defaults to `("kaiser", 8)`. Set to `None` if no windowing should be applied.
    dfreq: float (optional)
        Stepsize of the frequency sampling in cycles / pixel

    Returns
    -------
    freq:
        Frequency shells in cycles / pixel (bin centers).
    fsc:
        Fourier shell correlation corresponding to the frequencies in freq.
    nfreq:
        Number of (fft) sample points per shell in Fourier space.
    half_bit:
        Half-bit threshold curve. See :Heel_JSB_2005: for details.

    Example
    -------
    >>> import numpy as np
    >>> import matplotlib.pyplot as plt
    >>> import scipy.misc
    >>> image = scipy.misc.face()[:,:,2]
    >>> im1 = image + 500*np.random.random(image.shape)
    >>> im2 = image + 500*np.random.random(image.shape)
    >>> freq, fsc, nfreq, half_bit = fourier_shell_correlation(im1, im2)
    >>> plt.plot(freq, fsc, label='FSC')
    >>> plt.plot(freq, half_bit, label='half-bit threshold')
    >>> plt.xlabel('frequency / (cycles / pixel)')
    >>> plt.legend()
    """

    def preprocess(im):
        im = np.asarray(im)
        if window is not None:
            im = im * ndwindow(window, im.shape)
        return im

    f1 = fftn(preprocess(im1))
    f2 = fftn(preprocess(im2))

    # determine frequency bins
    dims = np.asarray(f1.shape)
    ndim = len(dims)
    if dfreq is None:
        dfreq = 1 / dims.min()
    freq = np.arange(0, 0.5 * np.sqrt(ndim), dfreq)  # requested sampling
    freq_grid = np.sqrt(sum([xi * xi for xi in fftfreqn(dims)]))
    bins = np.digitize(freq_grid, (freq[1:] + freq[:-1]) / 2)

    # results
    nfreq = np.bincount(bins.ravel(), minlength=len(freq))
    half_bit = half_bit_threshold(nfreq, ndim)

    nominator = np.bincount(
        bins.ravel(), weights=(f1.real * f2.real + f1.imag * f2.imag).ravel(), minlength=len(freq)
    )
    denom1 = np.bincount(
        bins.ravel(), weights=(f1.real * f1.real + f1.imag * f1.imag).ravel(), minlength=len(freq)
    )
    denom2 = np.bincount(
        bins.ravel(), weights=(f2.real * f2.real + f2.imag * f2.imag).ravel(), minlength=len(freq)
    )
    fsc = nominator / np.sqrt(denom1 * denom2)

    return freq, fsc, nfreq, half_bit


def half_bit_threshold(nfreq, ndim):
    """
    Half-bit threshold curve for the Fourier shell correlation

    TODO(Paul): Referenz der Formel ;)
    """
    return (0.2071 + 1.9102 / nfreq ** (1 / (ndim - 1))) / (
        1.2071 + 0.9102 / nfreq ** (1 / (ndim - 1))
    )
