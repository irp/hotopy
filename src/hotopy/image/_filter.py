from itertools import repeat
import numpy as np
import torch.fft
from torch import as_tensor
from scipy.signal import get_window
from scipy.fft import fft2, ifft2, rfft2, irfft2
from scipy.ndimage import fourier_gaussian

from ..utils import rfftshape, Padder


def _broadcast_to_dim(i):
    """
    Returns indices to broadcast to dimension i, i.e. indexing operation like [...,(i-times newaxis)]
    """
    return (...,) + tuple(repeat(np.newaxis, i))


def ndwindow(window, shape):
    """
    n-dim window of scipy.signal.get_window function.

    Parameters
    ----------
    window: str or tuple
        window type to use. See scipy.signal.get_window.
    shape: tuple
        Shape of window.

    Returns
    -------
    Window in shape `shape`.
    """
    w = np.ones(shape, dtype=np.float64)
    for dim, size in enumerate(reversed(shape)):
        w *= get_window(window, size)[_broadcast_to_dim(dim)]
    return w


def gaussian_filter(im, sigma, workers=None):
    r"""
    Apply Fourier Gaussian filter on im with size sigma (in pixel units).

    Cut-off frequency :math:`D_0` is related by:
        .. math:: \sigma = (2 \pi D_0)^{-1}.

    Notes
    -----
    Does *not* broadcast to stack of images. Needs to be applied by each image individually. E.g. with `multiprocessing`:
    """
    filt = fourier_gaussian(
        fft2(im, workers=workers), sigma
    )  # sigma = 1/(2*np.pi*D0), cut-off freq
    return ifft2(filt, workers=workers).real


def gaussian_bandpass2_real(x, sigma_low=None, sigma_high=None, workers=-1):
    # early exit if nothing is to do ...
    if sigma_low is None and sigma_high is None:
        return x

    rshape = rfftshape(x.shape)
    shape = x.shape
    kernel = np.ones(rshape, dtype=x.dtype)

    if sigma_high is not None:
        kernel = 1 - fourier_gaussian(kernel, sigma_high, n=x.shape[-1])
    if sigma_low is not None:
        kernel = fourier_gaussian(kernel, sigma_low, n=x.shape[-1])

    return irfft2(rfft2(x, s=shape, workers=workers) * kernel, s=shape, workers=workers)


class FourierFilter:
    @property
    def kernel(self):
        return self._kernel

    @kernel.setter
    def kernel(self, val):
        self._kernel = as_tensor(val, device=self.device, dtype=self.dtype)

    def __init__(self, shape, *args, real=True, norm=None, dtype=None, device=None, pad=0):
        self.real = real  # use rfft?
        self.norm = norm  # fft normalization
        self.dtype = dtype
        self.device = device
        self._kernel = None
        self.padder = Padder(shape, pad, mode="edge")
        self.shape = self.padder.padded_shape

        if real:
            self.kernel_shape = rfftshape(self.shape)
        else:
            self.kernel_shape = self.shape

        self._init_kernel(*args)

    def _fft(self, x):
        if self.real:
            return torch.fft.rfftn(x, s=self.shape, norm=self.norm)
        return torch.fft.fftn(x, s=self.shape, norm=self.norm)

    def _ifft(self, x):
        if self.real:
            return torch.fft.irfftn(x, s=self.shape, norm=self.norm)
        return torch.fft.ifftn(x, s=self.shape, norm=self.norm)

    def _init_kernel(self, *args):
        raise NotImplementedError

    def apply_filter(self, X):
        """
        Directly apply filter in Fourier space.

        .. Note:: ``X`` is assumed to be already Fourier transformed.
        """
        return X * self.kernel

    def __call__(self, x):
        """
        Apply filter to given image or stack of images.

        Parameters
        ----------
        x: Tensor, array_like
            Image(s) to filer. A stack of images need to be in shape ``(n, *s)`` where s is the ``shape`` argument of
            constructor call.

        Returns
        -------
        Tensor:
            Filtered image as Tensor. If NumPy ndarray is required, call ``.numpy()``.
        """
        x = as_tensor(x, device=self.device)
        x = self.padder(x)
        X = self._fft(x)
        Y = self.apply_filter(X)
        y = self._ifft(Y)
        return self.padder.crop(y)


class GaussianBlur(FourierFilter):
    def _init_kernel(self, sigma):
        if self.real:
            n = self.shape[-1]  # length of signal before real FFT
        else:
            n = -1  # complex FFT

        self.kernel = fourier_gaussian(np.ones(self.kernel_shape), sigma, n=n)


class GaussianBandpass(FourierFilter):
    """
    Gaussian band-pass Fourier filter.

    Multiplies the input image (or image-stack) with the Fourier transform of a Gaussian in Fourier space.

    Parameters
    ----------
    shape: tuple
        Shape of image(s) to be filtered. Without padding and without (optional) batch dimension.
    sigma_low: float, tuple
        Standard deviation of Gaussian used for kept lower frequencies. Supports standard deviation per axis given as
        tuple. See ``scipy.ndimage.fourier_gaussian`` for details.
    sigma_high: float, tuple
        Standard deviation of Gaussian used for blocked lower frequencies. Supports standard deviation per axis given as
        tuple. See ``scipy.ndimage.fourier_gaussian`` for details.

        .. Note::
            The high pass kernel is generated from the low pass by taking 1 - lowpass-kernel, i.e. swapping of
            blocked and passed frequencies.
    real: bool
        Input signal/image is real-valued (Default ``True``). If signal is complex, set ``False``.
    norm: None, str
        Normalization of FFT. Choices are ``"backward"`` or ``None``, i.e. the default, ``"forward"``, or ``"ortho"``
    dtype: torch.dtype
        datatype for the filter kernel.
    pad: int, tuple (optional)
        padding to be applied before applying the filter.
        accepts arguments like ``pad_width`` of ``numpy.pad``. Defaults to 0.

    Example
    -------
    >>> from hotopy.image import GaussianBandpass
    >>> import matplotlib.pyplot as plt
    >>> import numpy as np
    >>> image_stack = np.random.random((3, 64, 64)) + np.linspace(0, 2, 64)[None,:,None]
    >>> sigma = 1
    >>> pad = int(4 * sigma + 1)
    >>> # pad = 0  # comparison without padding
    >>> lowpass = GaussianBandpass(image_stack.shape[-2:], sigma, None, pad=pad)
    >>> highpass = GaussianBandpass(image_stack.shape[-2:], None, 2 * sigma, pad=pad)
    >>> bandpass = GaussianBandpass(image_stack.shape[-2:], sigma, 2 * sigma, pad=pad)
    >>> plt.subplot(221); plt.imshow(image_stack[0])
    >>> plt.subplot(222); plt.imshow(lowpass(image_stack)[0])
    >>> plt.subplot(223); plt.imshow(highpass(image_stack)[0])
    >>> plt.subplot(224); plt.imshow(bandpass(image_stack)[0])
    """

    def _init_kernel(self, sigma_low, sigma_high):
        if self.real:
            n = self.shape[-1]  # length of signal before real FFT
        else:
            n = -1  # complex FFT

        kernel = np.ones(self.kernel_shape)
        if sigma_high is not None:
            kernel = 1 - fourier_gaussian(kernel, sigma_high, n=n)
        if sigma_low is not None:
            kernel = fourier_gaussian(kernel, sigma_low, n=n)

        self.kernel = kernel


def dissect_levels(im, *lims, xp=None):
    """
    Dissect an image into sub-images containing only values from -inf <= lim1 < ... < limN < +inf

    Parameters
    ----------
    im: array
        Image
    *lims: float, Optional
        Limit values to dissect at. If no limits are given, the original image is returned with one additional axis
        prepended.
    xp: array_namespace
         Numpy, PyTorch, etc.

    Returns
    -------
    segments: array
        Dissected sub-images in shape ``(len(lims) + 1, *im.shape)``. I.e. for one limit this is ``(2, *im.shape)``.
    """
    xp = np if xp is None else xp

    lims = sorted(lims) + [float("+inf")]
    lower = float("-inf")

    masks = xp.zeros((len(lims), *im.shape), dtype=bool)

    for i, upper in enumerate(lims):
        masks[i] = (lower <= im) & (im < upper)
        lower = upper

    return im * masks
