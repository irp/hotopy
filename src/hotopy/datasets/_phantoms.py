"""
.. autosummary::
    :toctree: generated/


.. author: Jens Lucht, 2023
"""

from ._fetcher import fetcher
from ..image import dissect_levels


def _simple_phantom(name):
    cnt = fetcher(f"phantom_{name!s}.npz")
    return cnt["phantom"]


def dicty():
    return _simple_phantom("dicty")


# not an actual dataset, but simple wrapper to get a multi-component/material phantom
def dicty_multi(*lims):
    """
    Multi-component/material ``dicty`` phantom.

    See dissect_levels, dicty
    """
    im = dicty()
    return dissect_levels(im, *lims)


def world():
    return _simple_phantom("world").astype(float)


def world_uint():
    # dataset in original datatype
    return _simple_phantom("world")
