"""
.. autosummary::
    :toctree: generated/

.. author: Jens Lucht, 2024
"""

from ._fetcher import fetcher


def _simple_holograms(name):
    return fetcher(f"holograms_{name!s}.npz")


def radiodurans():
    return _simple_holograms("radiodurans")


def beads():
    return _simple_holograms("beads")


def macrophage():
    return _simple_holograms("macrophage")


def world_holograms():
    return _simple_holograms("world")


def spider():
    """
    Single-distance dataset for direct contrast aka TIE regime phase retrieval.
    """
    return _simple_holograms("spider")


def logo_holograms():
    """
    deep-holographic dataset
    """
    return _simple_holograms("logo")
