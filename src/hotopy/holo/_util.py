"""
Author: Jens Lucht
"""

from itertools import combinations
from scipy.signal import find_peaks
import numpy as np
from numpy import asarray
from skimage.registration import phase_cross_correlation
from matplotlib import pyplot as plt
from math import ceil

from ..image import imshiftscale, imscale, radial_power_spectrum, gaussian_bandpass2_real


def _freq2ctfminimum(freq, f, bd):
    # contrast_transfer = (np.sin(np.pi*arg_ctf) + betadelta * np.cos(np.pi*arg_ctf)) ** 2
    # sin(x) + a * cos(x) = 1/sqrt(1+a²) * sin(x + arctan(a))
    return freq * freq / f + np.arctan(bd)


def _ctfminimum2freq(minimum, f, bd):
    return np.sqrt((minimum - np.arctan(bd)) * f)


def _expand_border(left, right, by=0.03):
    width = right - left
    return left - by * width, right + by * width


def check_fresnel_number(
    image_vec, fresnel_number, betadelta=0, labels=None, ax=None, scale="minima", num_minima=None
):
    """display power spectral density with added ctf theory curve

    Parameters
    ----------
    image_vec : array-like
        Series of images to get power spectral density from.
    fresnel_number : float
        Fresnel number of the theory curve.
    betadelta: float (optional)
        Beta-delta-ratio of the theory curve.
    labels: array of str (optional)
        Labels for the images.
    ax: Axes (optional)
        Axes to display the plots in.
    scale: "minima" or "freq", optional
        Determines whether the CTF minima or the frequencies are displayed on an equidistant scale.
    num_minima: int (optional)
        number of CTF minima to display

    Example
    -------
    from hotopy import datasets
    from xraylab.image.stats import check_fresnel_number

    data = datasets.beads()
    print(list(data))
    i_dist = 1
    holo = data["holograms"][i_dist]
    fresnel_number = data["fresnelNumbers"][i_dist]
    betadelta = 0

    ax = check_fresnel_number(prj_rescaled[i_dist], fresnel_number_classic[i_dist], betadelta=betadelta, scale="freq")
    ax.set_ylim((None, 1e5))

    ax = check_fresnel_number(prj_rescaled[i_dist], fresnel_number_classic[i_dist], betadelta=betadelta, scale="minima")
    ax.set_ylim((None, 1e5))
    """

    image_vec = np.asarray(image_vec)
    if image_vec.ndim == 2:
        image_vec = image_vec[None, :, :]

    fresnel_number = np.broadcast_to(fresnel_number, len(image_vec))
    betadelta = np.broadcast_to(betadelta, len(image_vec))

    if scale not in ("minima", "freq"):
        raise ValueError("scale has to be either minima or freq")

    if scale != "minima":
        if np.any(fresnel_number - fresnel_number[0]) or np.any(betadelta - betadelta[0]):
            raise ValueError(
                'testing different fresnel_numbers and betadeltas is only allowed, when the theory curves are aligned. (scale="minima")'
            )

    if num_minima is None:
        num_minima = ceil(1 / (4 * np.min(fresnel_number)) + np.arctan(np.max(betadelta)))

    arg_ctf = np.arange(0, num_minima + 0.05, 0.05)
    contrast_transfer = np.sin(np.pi * arg_ctf) ** 2
    if scale == "freq":
        arg_ctf = _ctfminimum2freq(arg_ctf, fresnel_number[0], betadelta[0])

    if ax is None:
        fig, ax = plt.subplots()
    ax.set_title("power spectral density")
    ax.set_yscale("log")
    # ax.set_xlim(_expand_border(arg_ctf[0], arg_ctf[-1]))
    # ax.set_xlim((arg_ctf[0], arg_ctf[-1]))

    # theory curve
    ax2 = ax.twinx()
    ax2.semilogy(arg_ctf, contrast_transfer, "r--", label="CTF")
    ax2.set_ylim((0.01, 2))
    ax2.yaxis.set_ticks_position("none")
    ax2.yaxis.set_major_locator(plt.NullLocator())
    ax2.set_zorder(ax.get_zorder() - 1)
    ax.set_frame_on(False)

    if scale == "minima":
        freq2arg = lambda freq, f, bd: _freq2ctfminimum(freq, f, bd)  # noqa: E731
        ax.set_xlabel("CTF minimum")
    elif scale == "freq":
        freq2arg = lambda freq, f, bd: freq  # noqa: E731
        ax.set_xlabel("frequency / (cycles/px)")

    # data curve(s)
    labels = labels or [f"data {i_im}" for i_im in range(len(image_vec))]
    for i_im, (image, f, bd) in enumerate(zip(image_vec, fresnel_number, betadelta, strict=True)):
        psd, freq, _ = radial_power_spectrum(image)
        ax.plot(freq2arg(freq, f, bd), psd, "-", label=labels[i_im])

    # legend
    lines1, labels1 = ax.get_legend_handles_labels()
    lines2, labels2 = ax2.get_legend_handles_labels()
    ax.legend(lines2 + lines1, labels2 + labels1, loc="upper right")

    return ax


def rescale_defocus_series(
    series,
    mag,
    out=None,
    upsample_factor=10,
    normalization=None,
    regargs=None,
    transformargs=None,
    sigma_gaussfilt=None,
):
    """
    Rescale measurement series of same sample at different magnifications to maximal magnification and register
    and correct shift eventually. Used in multi-distance holographic reconstructions with cone beam geometry.

    Uses DFT registration algorithm proposed in [1]_.

    Parameters
    ----------
    series : array-like
        Series of images to scale and register stacked in first axis.
    mag : array-like
        Magnifications corresponding to series of images. Needs to be array of length `series.shape[0]`.
    out : array, optional
        Output array to place corrected images in. Need to be in shape and dtype of series.
    upsample_factor : int, optional
        Upsampling factor for registration. See [2]_. Defaults to 10.
    normalization : str, None, optional
        Normalization factor used in registration step. See [2]_ and Notes below.
        Defaults to `None` which is different from skimage's default `'phase'`.
        See Notes in [2]_, this normalization should be more robust to noise.
    regargs : dict, None, optional
        Additional keyword arguments passed to `skimage.registration.phase_cross_correlation`. See Notes.
    transformargs : dict, None, optional
        Additional keyword arguments passed to `imshiftscale`.
    sigma_gaussfilt: tuple[float] or None, optional
        Apply Gaussian filtering as preprocessing before registering or ``None`` skips filtering.

    Returns
    -------
    out : array
        Stack of corrected and scaled images.
    shifts : array
        Shifts registered relative to image with maximal magnification in units of magnified image.

    Notes
    -----
    Used `skimage.registration.phase_cross_correlation` [2]_. See here for additional information and references.

    Rotations later with maybe [3]_.

    Currently, no reconstruction of holograms for better registration. Subject to change.

    Example
    -------
    >>> # example not tested ;)
    >>> from os import cpu_count
    >>> from multiprocessing import Pool
    >>> import numpy as np
    >>> from scipy.fft import set_workers
    >>> from hotopy.holo import rescale_defocus_series
    >>> # [...] load scaled but flat-field corrected holograms into `holos_raw` and corresponding magnifications into
    ... # `mag`. `holos_raw.shape = (n_theta, n_distances, pixel_x, pixel_y)`.
    >>> n_cpu = cpu_count()
    >>> set_workers(2)
    >>> n_parallel = n_cpu // 2
    >>> def parallel_rescale(holos):
    ...     # return only the corrected holograms, set more options if needed
    ...     return rescale_defocus_series(holos, mag)[0]
    >>> with Pool(n_parallel) as p:
    ...     holos = p.map(parallel_rescale, holos_raw)
    >>> holos = np.asarray(holos)

    Note, the above example is not optimized in memory usage. A more elaborated script could make use shared memory
    (e.g. from Python's `multiprocessing` module) and use the inplace variant of this method by setting the `out`
    argument.

    References
    ----------
    .. [1] Manuel Guizar-Sicairos, Samuel T. Thurman, and James R. Fienup,
           "Efficient subpixel image registration algorithms,"
           Optics Letters 33, 156-158 (2008). :DOI:`10.1364/OL.33.000156`
    .. [2] https://scikit-image.org/docs/stable/api/skimage.registration.html#phase-cross-correlation
    .. [3] https://scikit-image.org/docs/stable/auto_examples/registration/plot_register_rotation.html#sphx-glr-auto-examples-registration-plot-register-rotation-py
    """
    regargs = regargs or {}
    transformargs = transformargs or {}

    preprocess = None
    if sigma_gaussfilt is not None:
        sigmas = tuple(sigma_gaussfilt)
        if len(sigmas) != 2:
            raise ValueError("Only two sigmas (low, high) are allowed.")

        def preprocess(x):
            return gaussian_bandpass2_real(x, *sigmas)

    max_ind = np.argmax(mag)
    max_mag = mag[max_ind]
    n = len(mag)

    scales = max_mag / asarray(mag)
    ref = asarray(series[max_ind])

    # preprocess reference image
    if preprocess is not None:
        ref = preprocess(ref)

    # allocate output memory
    # temporary image needed, if output=input (i.e. inplace) since original input needed in imshiftscale again
    tmp = np.empty_like(ref)
    if out is None:  # if output not given, preallocate memory
        out = np.empty((n, *ref.shape), dtype=ref.dtype)
    shifts = np.zeros((n, ref.ndim))
    errors = np.zeros((n,))

    # scale series to common maximal magnification
    for i, (im, scale) in enumerate(zip(series, scales, strict=True)):
        if i == max_ind:
            # no need to rescale and register reference image, shifts and error remain zero
            out[i] = series[i]
        else:
            # first rescale to common scaling before applying preprocessing
            imscale(im, scale, output=tmp)

            if preprocess is not None:
                tmp = preprocess(tmp)

            # register reference image and rescaled and preprocessed image
            shifts[i], errors[i] = phase_cross_correlation(
                ref,
                tmp,
                upsample_factor=upsample_factor,
                normalization=normalization,
                **regargs,
            )[:2]

            # correct input series to maximal magnification and correct shifts eventually
            imshiftscale(im, shifts[i], scale, output=out[i], **transformargs)

    return out, shifts, errors


def rescale_defocus_fresnel_numbers(fresnel_nums, mag):
    """
    Rescales fresnel numbers accordingly to magnifications `mag`. in cone beam geometry to effective parallel beam
    setup.

    Parameters
    ----------
    fresnel_nums : array-like
        Fresnel numbers in cone beam setup
    mag : array-like
        Magnification factors

    Returns
    -------
    fresnel_nums : array
        Fresnel numbers in effective parallel beam.
    """
    mag = asarray(mag)
    fresnel_nums = asarray(fresnel_nums)
    return fresnel_nums * (mag / max(mag)) ** 2


def find_fresnel_number(
    x, prominence=0.3, flow=None, fhigh=None, guess=None, max_order=16, stats="median"
):
    """
    Fresnel number determination from given hologram ``x``.

    NOTE: Experimental function, likely to change.

    Parameters
    ----------
    x: array_like
        Hologram to determine Fresnel number of.
    prominence: float, tuple[float] or None, optional
        Required prominence of a peak. Vary this parameter to filter out false peaks (here minima). ``None`` disables
        prominence filters.
    flow: float or None, optional
        Lower frequency limit. If ``None`` and guess given, set to half of first pure phase CTF root.
    fhigh: float or None, optional
        Higher frequencies limit. If ``None`` and guess given, set to max_order-th root of pure phase CTF.
    guess: float or optional
        Initial guess for Fresnel number, used for masking of power spectrum.
    max_order: int, optional
        Cutoff after ``max_order`` pure phase (sin-)CTF roots. Only possible if ``guess`` is given.
    stats: None or str, optional
        Statistical reduction method to use: either ``'median'`` (default, equivalent to ``None``) or ``'mean'``.
        Median is more robust against outliers, mean is more robust against (slightly) peak position errors.

    Returns
    -------
    fresnel_number : float
        Determined Fresnel number
    freq_minima: array
        Frequencies of found minima.
    peak_properties: tuple
        Properties of peak.


    Example
    -------
    TODO

    Notes
    -----
    This is *not* are fire and forget function. Check your results and eventually adapt parameters to your needs.
    """
    psd, freq = radial_power_spectrum(x)[:2]

    if guess is not None:
        # lower limit to prevent noisy low-freq signal to influence too much
        flow = flow or np.sqrt(1 / 2 * guess)
        if max_order is not None:
            fhigh = fhigh or np.sqrt(max_order * guess)

    mask = np.ones_like(freq).astype(bool)
    if flow is not None:
        mask &= freq >= flow
    if fhigh is not None:
        mask &= freq <= fhigh

    y = np.log(psd[mask])
    (xp, pp) = find_peaks(-y, prominence=prominence)

    freq_xp = freq[mask][xp]
    diff = np.diff(freq_xp**2)

    if stats is None or str(stats).lower() == "median":
        fn = np.median(diff)
    elif str(stats).lower() == "mean":
        fn = np.mean(diff)
    else:
        raise ValueError("Illegal stats value. Possible values: None, 'median', or 'mean'.")

    return fn, freq_xp, pp


def fresnel_number_pairs(fresnel_num_list):
    """
    List of all two-element combinations for given list of Fresnel numbers.

    .. Note:: Does not support astigmatism.
    """
    return list(combinations(fresnel_num_list, 2))


def difference_fresnel_numbers(fresnel_nums):
    """
    Returns all possible difference Fresnel number for given Fresnel numbers ``fresnel_nums``.

    .. Note:: Does not support astigmatism.
    """

    def compute_diff_fnum(pair):
        """Compute difference Fresnel number given pair (f1, f2)"""
        f1, f2 = pair
        return 1 / abs(1 / f1 - 1 / f2)

    pairs = fresnel_number_pairs(fresnel_nums)
    return np.array(list(map(compute_diff_fnum, pairs)))
