"""
ASTRA Toolbox wrappers
======================

The ASTRA Toolbox is a MATLAB and Python toolbox of high-performance GPU primitives for 2D and 3D tomography [1]_, [2]_.

References
----------

.. [1] http://www.astra-toolbox.com
.. [2] https://github.com/astra-toolbox/astra-toolbox

.. currentmodule:: hotopy.astra


High-level tomographic wrappers
------------------------------
.. autosummary::
    :toctree: generated/

    iradon_parallel

..
    author: Jens Lucht, 2022
"""

import numpy as np
import astra
import math
import copy
import logging
from skimage.registration import phase_cross_correlation
from scipy.ndimage import gaussian_filter

__all__ = [
    "AstraWrapper",
    "iradon_parallel",
]

logger = logging.getLogger(__name__)


def noop(x):
    return x


class AstraWrapper:
    """
    ASTRA base wrapper for 3d data sets
    """

    def __init__(self, proj_geom, vol_geom, alg_cfg: dict, sino_pad=0):
        self.alg_cfg = alg_cfg
        self.sino_pad = sino_pad

        # data handling
        self.rec_id = self.data.create("-vol", vol_geom)  # only one slice
        self.proj_id = self.data.create("-sino", proj_geom)

        # add data to algo configuration
        alg_cfg["ReconstructionDataId"] = self.rec_id
        alg_cfg["ProjectionDataId"] = self.proj_id

        self.alg_id = astra.algorithm.create(alg_cfg)

    def rec_proj(self, proj=None, niter=1, get_data=True):
        """Single projection reco."""
        if proj is not None:
            self.load_proj(proj)
        astra.algorithm.run(self.alg_id, niter)

        if get_data:
            return self.data.get(self.rec_id)

    def get_proj_geom(self):
        return self.data.get_geometry(self.proj_id)

    def load_proj_geom(self, new_geom):
        """Replace the projection geometry."""
        self.data.change_geometry(self.proj_id, new_geom)

    def correct_shift(self, shift, reference_geom=None):
        """Correct geometry for detector shift."""
        if reference_geom is None:
            reference_geom = self.get_proj_geom()
        else:
            reference_geom = copy.deepcopy(reference_geom)
        self.load_proj_geom(geom_postalignment(reference_geom, shift))

    def rotate_detector(self, angle_deg, reference_geom=None):
        if reference_geom is None:
            reference_geom = self.get_proj_geom()
        else:
            reference_geom = copy.deepcopy(reference_geom)
        self.load_proj_geom(geom_rotate_detector(reference_geom, angle_deg))

    def adjust_nick(self, angle_deg, reference_geom=None):
        if reference_geom is None:
            reference_geom = self.get_proj_geom()
        else:
            reference_geom = copy.deepcopy(reference_geom)
        self.load_proj_geom(geom_adjust_nick(reference_geom, angle_deg))

    def load_volume(self, vol_data):
        self.data.store(self.rec_id, vol_data)

    def get_volume(self):
        return self.data.get(self.rec_id)

    def __call__(self):
        raise NotImplementedError(
            f"Meta class '{self.__class__.__name__}' shall not called directly"
        )

    def __del__(self):
        self.data.delete(self.rec_id)
        self.data.delete(self.proj_id)
        astra.algorithm.delete(self.alg_id)


class AstraWrapper2D(AstraWrapper):
    ndim = 2
    data = astra.data2d

    def load_proj(self, proj):
        if self.sino_pad > 0:
            proj = np.pad(proj, ((0, 0), (self.sino_pad, self.sino_pad)), mode="edge")

        self.data.store(self.proj_id, proj)

    def __call__(self, projs, slices=None, niter=1, loop_wrap=None):
        loop_wrap = loop_wrap or noop

        ny, nx = projs.shape[-2:]
        if slices is None:
            slices = range(ny)
            nslices = ny
        else:
            nslices = len(slices)

        # reco target
        target = np.empty((nslices, nx, nx), dtype=np.float32)

        for i, yi in loop_wrap(enumerate(slices)):
            self.data.store(self.rec_id, 0)
            target[i] = self.rec_proj(projs[:, yi, :], niter=niter)

        return target

    def fproject(self):
        fpalg_cfg = astra.astra_dict("FP_CUDA")
        fpalg_cfg["VolumeDataId"] = self.rec_id
        fpalg_cfg["ProjectionDataId"] = self.proj_id
        fpalg_id = astra.algorithm.create(fpalg_cfg)

        astra.algorithm.run(fpalg_id)
        astra.algorithm.delete(fpalg_id)

        return self.get_proj()

    def get_proj(self):
        if self.sino_pad == 0:
            return self.data.get(self.proj_id).swapaxes(0, 1)
        else:
            return self.data.get(self.proj_id)[:, self.sino_pad : -self.sino_pad]


class AstraWrapper3D(AstraWrapper):
    ndim = 3
    data = astra.data3d

    def __call__(self, projs=None, proj_geom=None, niter=1, get_data=True):
        if projs is not None:
            self.load_proj(projs)
        if proj_geom is not None:
            self.load_proj_geom(proj_geom)
        return self.rec_proj(niter=niter, get_data=get_data)

    def fproject(self):
        fpalg_cfg = astra.astra_dict("FP3D_CUDA")
        fpalg_cfg["VolumeDataId"] = self.rec_id
        fpalg_cfg["ProjectionDataId"] = self.proj_id
        fpalg_id = astra.algorithm.create(fpalg_cfg)

        astra.algorithm.run(fpalg_id)
        astra.algorithm.delete(fpalg_id)

        return self.get_proj()

    def apply_vol_constraints(self, support=None):
        if support is not None:
            self.data.store(self.rec_id, support * self.data.get(self.rec_id))

    def load_proj(self, proj):
        if self.sino_pad > 0:
            proj = np.pad(proj, ((0, 0), (0, 0), (self.sino_pad, self.sino_pad)), mode="edge")

        self.data.store(self.proj_id, proj.swapaxes(0, 1))

    def get_proj(self):
        if self.sino_pad == 0:
            return self.data.get(self.proj_id).swapaxes(0, 1)
        else:
            return self.data.get(self.proj_id).swapaxes(0, 1)[:, :, self.sino_pad : -self.sino_pad]

    def get_angles(self):
        """For 3D cone geometries, this returns athe angles of the ray directions projected onto a horizontal plane"""
        geom = self.get_proj_geom()

        if geom["type"] == "cone_vec":
            v = geom["Vectors"]
            return (np.arctan2(v[:, 1] - v[:, 4], v[:, 0] - v[:, 3]) + np.pi / 2) % (2 * np.pi)
        elif geom["type"] == "cone":
            return self.get_proj_geom()["ProjectionAngles"]
        else:
            raise NotImplementedError()


def iradon_parallel(imdim, angles, dx=1.0, alg="FBP_CUDA", filter=None, sino_pad=0, **alg_cfg):
    """
    ASTRA reco for parallel beam geometry.

    Parameters
    ----------
    imdim: tuple
        `(ny, nx)`
    angles: array-like
        Tomographic angles in radians.
    dx: float
        Pixel size in x.
    alg: str, Optional
        Algorithm to use. Supported `FBP_CUDA`, `SART_CUDA`, `SIRT_CUDA`, `CGLS_CUDA`. Defaults to `FBP_CUDA`.
    filter: str, None, Optional
        Filter to use for filtered backprojection (FBP). Ignored when other algorithm is used. Defaults to `ram-lak` if
        `FBP` or `FBP_CUDA` is used otherwise `None`.

        Available filters:
            ``none, ram-lak, shepp-logan, cosine, hamming, hann, tukey, lanczos,
            triangular, gaussian, barlett-hann, blackman, nuttall, blackman-harris,
            blackman-nuttall, flat-top, kaiser, parzen``

    Example
    -------
    >>> from hotopy.astra import iradon_parallel
    >>> # FBP reconstruction
    >>> iradon = iradon_parallel(projs.shape[-2:], angles, filter="shepp-logan")  # default FBP_CUDA algorithm
    >>> vol_fbp = iradon(projs)

    >>> # SIRT reconstruction
    >>> from tdqm import tqdm  # optional, for nice progress report
    >>> iradon_sirt = iradon_parallel(projs.shape[-2:], angles, alg="SIRT_CUDA")
    >>> ny = projs.shape[-2]
    >>> slices = np.arange(ny//2-5, ny//2+5+1, dtype=int)  # central 10 slices
    >>> vol_sirt = iradon_sirt(projs, slices, niter=16, loop_wrap=tdqm)  # using 16 iterations
    """
    # algorithm defaults
    alg_defaults = {
        "type": alg,
        "option": {
            "FilterType": filter or "Ram-Lak",
        },
    }
    alg_cfg = {**alg_defaults, **alg_cfg}

    if not str(alg).upper().startswith("FBP"):
        del alg_cfg["option"]["FilterType"]  # unused option if not FBP

    nx = imdim[-1]
    ncol = nx + 2 * sino_pad  # detector columns plus padding

    proj_geom = astra.create_proj_geom("parallel", dx, ncol, angles)
    vol_geom = astra.create_vol_geom(nx, nx)

    return AstraWrapper2D(proj_geom, vol_geom, alg_cfg, sino_pad=sino_pad)


def fanflat(imdim, angles, z01, z02, dx, sino_pad=0):
    npx = imdim[-1]
    ncol = npx + 2 * sino_pad  # detector columns plus padding

    # effective geometry
    M = z02 / z01
    dx_eff = dx / M
    z01_eff = z01 / dx_eff
    z12_eff = (z02 - z01) / dx_eff

    proj_geom = astra.create_proj_geom("fanflat", M, ncol, angles, z01_eff, z12_eff)
    vol_geom = astra.create_vol_geom(npx, npx)
    alg_cfg = astra.astra_dict("FBP_CUDA")

    return AstraWrapper2D(proj_geom, vol_geom, alg_cfg, sino_pad=sino_pad)


def fdk(imdim, angles, z01, z02, dx, nslices=None, sino_pad=0):
    ny, nx = imdim
    ncol = nx + 2 * sino_pad  # detector columns plus padding
    nslices = nslices or ny  # all slices if not given

    # effective geometry
    M = z02 / z01
    dx_eff = dx / M
    z01_eff = z01 / dx_eff
    z12_eff = (z02 - z01) / dx_eff

    # cone beam geometry
    cone_geom = astra.create_proj_geom("cone", M, M, ny, ncol, angles, z01_eff, z12_eff)

    # reconstruction volume
    vol_geom = astra.create_vol_geom(nx, nx, nslices)  # in (y, x, z)

    # algorithm
    alg_cfg = astra.astra_dict("FDK_CUDA")

    return AstraWrapper3D(cone_geom, vol_geom, alg_cfg, sino_pad)


def parallel3d(imdim, angles, nslices=None, sino_pad=0, z01=80000, z12=30):
    ny, nx = imdim
    ncol = nx + 2 * sino_pad  # detector columns plus padding
    nslices = nslices or ny  # all slices if not given

    # cone beam geometry
    # z01 = 80000
    # z12 = 30
    logger.warning(f"currently parallel3d is implemented as fdk with a narrow cone\n{z01=}, {z12=}")
    cone_geom = astra.create_proj_geom("cone", 1, 1, ny, ncol, angles, z01, z12)

    # reconstruction volume
    vol_geom = astra.create_vol_geom(nx, nx, nslices)  # in (y, x, z)

    # algorithm
    alg_cfg = astra.astra_dict("FDK_CUDA")

    return AstraWrapper3D(cone_geom, vol_geom, alg_cfg, sino_pad)


def sirt3d(imdim, angles, z01, z02, dx, nslices=None, sino_pad=0):
    ny, nx = imdim
    ncol = nx + 2 * sino_pad  # detector columns plus padding
    nslices = nslices or ny  # all slices if not given

    # effective geometry
    M = z02 / z01
    dx_eff = dx / M
    z01_eff = z01 / dx_eff
    z12_eff = (z02 - z01) / dx_eff

    # cone beam geometry
    cone_geom = astra.create_proj_geom("cone", M, M, ny, ncol, angles, z01_eff, z12_eff)

    # reconstruction volume
    vol_geom = astra.create_vol_geom(nx, nx, nslices)  # in (y, x, z)

    # algorithm
    alg_cfg = astra.astra_dict("SIRT3D_CUDA")

    return AstraWrapper3D(cone_geom, vol_geom, alg_cfg, sino_pad)


def extract_shifts(proj_geom):
    # see https://www.astra-toolbox.com/docs/geom3d.html for V components (-1 for python)
    raise NotImplementedError(
        (
            "sample shifts arent easily extracted from geometry.",
            "which contains a whole trajectory for the detector",
        )
    )


def geom_postalignment(proj_geom, factor, move="detector"):
    """Extends astra.functions.geom_postalignment to handle factors per angle.
    Apply a postalignment to a vector-based projection geometry.
    Can be used to model the rotation axis offset or shifts of the sample.

    For 2D geometries, the argument factor is a single float specifying the
    distance to shift the detector (measured in detector pixels).

    For 3D geometries, factor can be a pair of floats specifying the horizontal
    resp. vertical distances to shift the detector. If only a single float
    is specified, this is treated as a horizontal shift.
    factor.shape: interpretation
    (1,): horizontal shift
    (2,): (horizontal shift, vertical shift)
    (num_angles,): horizontal shift per angle
    (num_angles, 2): horizontal and vertical shift per angle

    :param proj_geom: input projection geometry
    :type proj_geom: :class:`dict`
    :param factor: number of pixels to shift the detector (can be a vector now)
    :type factor: :class:`float`
    """

    proj_geom = geom_2vec(proj_geom)
    V = proj_geom["Vectors"]
    factor = np.atleast_1d(factor)

    # expand factor dimension
    if factor.ndim == 1 and len(factor) > 2:
        factor = factor[:, None]

    if proj_geom["type"] == "parallel_vec" or proj_geom["type"] == "fanflat_vec":
        V[:, 2:4] = V[:, 2:4] + factor * V[:, 4:6]
    elif proj_geom["type"] == "parallel3d_vec":
        V[:, 3:6] = V[:, 3:6] + factor[..., 0, None] * V[:, 6:9]
        if factor.shape[-1] > 1:
            V[:, 3:6] = V[:, 3:6] + factor[..., 1, None] * V[:, 9:12]
    elif proj_geom["type"] == "cone_vec":
        if move == "detector":
            V[:, 3:6] = V[:, 3:6] + factor[..., 0, None] * V[:, 6:9]
            if factor.shape[-1] > 1:
                V[:, 3:6] = V[:, 3:6] + factor[..., 1, None] * V[:, 9:12]
        elif move == "sample":
            shift = -factor[..., 0, None] * V[:, 6:9]
            if factor.shape[-1] > 1:
                shift -= factor[..., 1, None] * V[:, 9:12]
            V[:, 0:3] += shift
            V[:, 3:6] += shift
    else:
        raise RuntimeError("No suitable geometry for postalignment: " + proj_geom["type"])

    return proj_geom


def rotation_matrix(axis, theta):
    """adapted from https://stackoverflow.com/questions/6802577/rotation-of-3d-vector
    Return the rotation matrix associated with counterclockwise rotation about
    the given axis by theta radians.
    """
    axis = np.asarray(axis)
    axis = axis / math.sqrt(np.dot(axis, axis))
    a = math.cos(theta / 2.0)
    b, c, d = -axis * math.sin(theta / 2.0)
    aa, bb, cc, dd = a * a, b * b, c * c, d * d
    bc, ad, ac, ab, bd, cd = b * c, a * d, a * c, a * b, b * d, c * d
    return np.array(
        [
            [aa + bb - cc - dd, 2 * (bc + ad), 2 * (bd - ac)],
            [2 * (bc - ad), aa + cc - bb - dd, 2 * (cd + ab)],
            [2 * (bd + ac), 2 * (cd - ab), aa + dd - bb - cc],
        ]
    )


def geom_rotate_detector(proj_geom, angle_deg):
    """
    rotate the detector around the ray direction. angle_deg can be a vector
    """
    proj_geom = geom_2vec(proj_geom)

    if proj_geom["type"] not in ("parallel3d_vec", "cone_vec"):
        raise RuntimeError("No suitable geometry for geom_rotate_detector: " + proj_geom["type"])

    V = proj_geom["Vectors"]
    angle_deg = np.broadcast_to(angle_deg, len(V))
    for i_proj in range(len(V)):
        rot = rotation_matrix(V[i_proj, 0:3], np.deg2rad(angle_deg[i_proj]))
        V[i_proj, 6:9] = rot @ V[i_proj, 6:9]
        V[i_proj, 9:12] = rot @ V[i_proj, 9:12]

    return proj_geom


def geom_adjust_nick(proj_geom, angle_deg):
    """
    shift and rotate source and detector corresponding to a misaligned nick-angle
    of the rotation axis. The nick rotation is orthogonal to the beam and to z
    (3rd coordinate). angle_deg can be a vector
    """
    proj_geom = geom_2vec(proj_geom)

    if proj_geom["type"] not in ("parallel3d_vec", "cone_vec"):
        raise RuntimeError("No suitable geometry for geom_adjust_nick: " + proj_geom["type"])

    V = proj_geom["Vectors"]
    angle_deg = np.broadcast_to(angle_deg, len(V))
    for i_proj in range(len(V)):
        beam_direction = V[i_proj, 3:6] - V[i_proj, 0:3]
        nick_direction = np.cross(beam_direction, (0, 0, 1))
        rot_mat = rotation_matrix(nick_direction, np.deg2rad(angle_deg[i_proj]))
        V[i_proj, 0:3] = rot_mat @ V[i_proj, 0:3]
        V[i_proj, 3:6] = rot_mat @ V[i_proj, 3:6]
        V[i_proj, 6:9] = rot_mat @ V[i_proj, 6:9]
        V[i_proj, 9:12] = rot_mat @ V[i_proj, 9:12]
    return proj_geom


def geom_2vec(proj_geom):
    """Convert geometry to vector type"""
    if proj_geom["type"].endswith("_vec"):
        return proj_geom
    else:
        return astra.geom_2vec(proj_geom)


def reproj_alignment(
    reconstructor,
    shift_tolerance=0.1,
    max_iterations=20,
    proj_filter="highpass",
    highpass=20,
    loop_wrap=None,
    init_shifts=None,
):
    """
    proj_filter: filter function to apply to projections before registration. default: highpass filter
    for now, a copy of both projections and reprojections is kept in astra memory
    Example usage:
    import hotopy.astra as hastra
    reconstructor = hastra.fdk(data_sel.shape[-2:], angles_sel, z01, z02, dx)
    reconstructor.correct_shift((10, 0))
    reconstructor.load_proj(data_sel)
    uncorrected_vol = reconstructor()
    shifts, rotAngles, geom_corr, tmp_out = hastra.reproj_alignment(reconstructor, niter=4, loop_wrap=tqdm)
    corrected_vol = reconstructor(proj_geom=geom_corr)
    """
    loop_wrap = loop_wrap or noop

    if proj_filter == "highpass":

        def proj_filter(projs):
            return np.array([prj - gaussian_filter(prj, highpass) for prj in loop_wrap(projs)])

    elif proj_filter == "id":  # identity

        def proj_filter(projs):
            return projs

    else:
        assert callable(proj_filter)

    geom_uncorrected = reconstructor.get_proj_geom().copy()
    projections = reconstructor.get_proj()
    logger.debug("filter projections")
    projections_f = proj_filter(projections)
    numangles = projections.shape[0]
    angles = reconstructor.get_angles()
    s = np.sin(angles)
    s /= np.linalg.norm(s)
    c = np.cos(angles)
    c /= np.linalg.norm(c)

    # forward projection algorithm
    if init_shifts is None:
        total_shifts = np.zeros((numangles, 2))
    else:
        total_shifts = init_shifts

    all_shifts = []
    for i_it in loop_wrap(range(max_iterations)):
        # update geometry
        reconstructor.correct_shift(total_shifts, reference_geom=geom_uncorrected)

        # reconstruction
        logger.debug("reconstruction")
        reconstructor.rec_proj()

        # forward projection
        logger.debug("forward projection")
        reprojections_f = proj_filter(reconstructor.fproject())

        # registration
        logger.debug("shift registration")
        shifts = np.zeros((numangles, 2))
        for i_proj in loop_wrap(range(numangles)):
            shifts[i_proj] = -np.flip(
                phase_cross_correlation(
                    projections_f[i_proj],
                    reprojections_f[i_proj],
                    upsample_factor=20,
                    normalization=None,
                )[0]
            )

        # remove shift components that correspond to translation of the sample
        shifts[:, 1] -= shifts[:, 1].mean()  # vertical
        shifts[:, 0] -= np.dot(s, shifts[:, 0]) * s + np.dot(c, shifts[:, 0]) * c  # horizontal

        all_shifts.append(shifts)
        max_shift = np.sqrt((shifts * shifts).sum(1).max())
        logger.info(f"it {i_it}: max shift: {max_shift:.2f} pixels")
        total_shifts += shifts

        reconstructor.load_proj(projections)
        if max_shift <= shift_tolerance:
            break
    else:
        logger.warning(f"WARNING: shift registration did not converge, last max_shift: {max_shift}")

    geom_corrected = copy.deepcopy(reconstructor.get_proj_geom())
    reconstructor.load_proj_geom(geom_uncorrected)  # revert back to original geom
    all_shifts = np.array(all_shifts)
    return total_shifts, 0, geom_corrected, all_shifts
