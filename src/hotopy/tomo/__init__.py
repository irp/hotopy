"""
========================================
Tomographic methods (:mod:`hotopy.tomo`)
========================================

Tomographic reconstruction
--------------------------
.. autosummary::
    :toctree: generated/

    iradon_parallel


Ring removal
------------
.. autosummary::
    :toctree: generated/

    ringremove
    ringremove_additive
    ringremove_wavelet


Tomographic corrections and alignment
-------------------------------------
.. autosummary::
    :toctree: generated/

    find_sample_rotaxis_shift
    sample_rotaxis_shift_correlation


Consistency checks
------------------
.. autosummary::
    :toctree: generated/

    hlcc
"""

from logging import getLogger

from ._alignment import (
    hlcc,
    find_sample_rotaxis_shift,
    sample_rotaxis_shift_correlation,
    register_sinogram,
)
from ._ringremove import ringremove, ringremove_wavelet, ringremove_additive


logger = getLogger(__name__)

# only load astra-depended methods if astra is available
try:
    from ._astra import (
        AstraWrapper,
        AstraWrapper2D,
        AstraWrapper3D,
        iradon_parallel,
        fdk,
        fanflat,
        parallel3d,
        sirt3d,
        geom_postalignment,
        geom_rotate_detector,
        geom_2vec,
        reproj_alignment,
    )

    __has_astra__ = True
    logger.info(f"Loaded {__name__} with ASTRA sub-module")
except ModuleNotFoundError as err:
    __has_astra__ = False
    logger.warning(
        f"Could not import 'astra' package for tomographic operations. See https://astra-toolbox.com for installation "
        f"guidance."
        f"Loaded {__name__} without ASTRA sub-module. ASTRA-depended functions are not available."
    )
    logger.debug(err)


__all__ = [
    # alignment
    "hlcc",
    "find_sample_rotaxis_shift",
    "sample_rotaxis_shift_correlation",
    "register_sinogram",
    # ringremove
    "ringremove",
    "ringremove_additive",
    "ringremove_wavelet",
]

if __has_astra__:
    __all__ += [
        # astra
        "AstraWrapper",
        "AstraWrapper2D",
        "AstraWrapper3D",
        "iradon_parallel",
        "fdk",
        "fanflat",
        "parallel3d",
        "sirt3d",
        "geom_postalignment",
        "geom_rotate_detector",
        "geom_2vec",
        "reproj_alignment",
    ]
