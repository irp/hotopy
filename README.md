HoToPy - Holographic and tomographic toolbox for X-ray imaging in Python

# HoToPy

## Installation

### pip installation

HoToPy can be installed through pip by running
```commandline
pip install hotopy --extra-index-url https://gitlab.gwdg.de/api/v4/projects/17398/packages/pypi/simple
```
for a basic HoToPy installation. This basic version does not contain the [`astra-toolbox`][astra] for tomographic
reconstructions. 

If you intend to use the _tomographic reconstruction functions_ in `hotopy.tomo` you can install its dependencies
either by running
```commandline
pip install hotopy[tomo] --extra-index-url https://gitlab.gwdg.de/api/v4/projects/17398/packages/pypi/simple
```
or you can have a look at the [astra-toolbox][astra] documentation for an installation method that fits your needs.

**Note**: Example datasets in `hotopy.datasets` are only downloaded upon invocation.

### conda environment

You can alternatively install HoToPy's dependencies in a conda environment.
Here we use [micromamba][mircomamba], a drop-in replacement for conda. 

Set up an HoToPy environment with:
```commandline
micromamba create --name hotopy \
 -c astra-toolbox -c pytorch -c nvidia -c conda-forge --channel-priority 1 \
 pytorch pytorch-cuda 'numpy>=2' scipy scikit-image h5py matplotlib pooch tqdm astra-toolbox
```
Then activate the environment and install HoToPy via pip therein:
```commandline
micromamba activate hotopy
```
```commandline
pip install hotopy --extra-index-url https://gitlab.gwdg.de/api/v4/projects/17398/packages/pypi/simple
```

**Note ASTRA**: The last package `astra-toolbox` is optional. If you do not intend to use the tomography part you can omit it.

## Getting started

You can find examples in the dedicated [HoToPy-Examples repository](https://gitlab.gwdg.de/irp/hotopy-examples).

## Documentation

https://irp.pages.gwdg.de/hotopy/

## How to contribute

Generally checkout the [NumPy developer docs](https://numpy.org/doc/stable/dev/howto-docs.html) and setup your git first.

Clone the repository and setup pre-commit hooks
```commandline
git clone git@gitlab.gwdg.de:irp/hotopy.git
cd hotopy
```

(Optional) Create a venv and launch it to your current shell
```commandline
python3 -m venv venv --prompt HoToPy
source venv/bin/activate
```

Install `hotopy` module in editable mode (note: you may add the `--user` flag if you are not using venv's)
```commandline
pip install -e '.[dev,docs,tomo]'
pre-commit install
```

Make a new branch for the new feature / fix you want to develop.
```commandline
git checkout -b <usefulandshortname>
```

Write the code. Make sure by running pre-commit it does not raise errors or warnings. You can check this anytime
by running 
```commandline
pre-commit
```

Push your code to the corresponding branch in the remote repository.
```commandline
git push -u origin HEAD
```

At this point other people can see your code, but it is not part of the master branch, so you can get feedback and help without having to worry about breaking anything.

When you are happy and think, your adjustments should be included in the master
branch, create a merge request on the Gitlab website. Someone else will then check your code and merge it into the master branch if appropriate.

Only when you know what you are doing and are certain not to break anything, small changes can also be pushed directly to the master branch.



[holotomo]: https://gitlab.gwdg.de/irp/holotomotoolbox
[astra]: https://astra-toolbox.com/
[mircomamba]: https://mamba.readthedocs.io/en/latest/installation/micromamba-installation.html
